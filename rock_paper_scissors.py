# 0: sasso
# 1: carta
# 2: forbice

import random

# r_num = random.randint(0, 2)
# print(r_num)



#### Facciamo dei test e vedremo che la composizione casuale delle scelte del pc è uniforme
# occurrencies = {
#     'sasso': 0,
#     'carta': 0,
#     'forbice': 0,
# }

# for i in range(0, 1000):
#     r = random.randint(0, 2)
#     if r == 0:
#         occurrencies['sasso'] += 1
#     elif r == 1:
#         occurrencies['carta'] += 1
#     else:
#         occurrencies['forbice'] += 1

# print(occurrencies)
####


user_score = 0
pc_score = 0

game = True
while game:
    print("0 - Sasso")
    print("1 - Carta")
    print("2 - Forbice")
    print("3 - Resetta il punteggio")
    print("4 - Termina il gioco")

    user_input = input('\nCosa vuoi fare?\n')

    if user_input == '4':
        print('Gioco terminato!')
        print(f"Punteggio giocatore umano: {user_score}")
        print(f"Punteggio computer: {pc_score}")
        if int(user_score) > int(pc_score):
            print("Complimenti! Hai vinto!")
        else:
            print("Mi dispiace! Questa volta non hai vinto...")
        game = False
    
    if user_input == '3':
        print('Resetto i punteggi!')
        user_score = 0
        pc_score = 0
        print(f"Punteggio giocatore umano: {user_score}")
        print(f"Punteggio computer: {pc_score}")

    if user_input == '2':
        print('Hai scelto forbice')
        pc_choice = random.randint(0, 2)
        if pc_choice == 0:
            print('Il computer ha scelto sasso')
            print('Il computer vince!')
            pc_score += 1
        elif pc_choice == 1:
            print('Il computer ha scelto carta')
            print('Hai vinto!')
            user_score += 1
        elif pc_choice == 2:
            print('Il computer ha scelto forbice')
            print('E\' patta!')

    if user_input == '1':
        print('Hai scelto carta')
        pc_choice = random.randint(0, 2)
        if pc_choice == "0":
            print('Il computer ha scelto sasso')
            print('Hai vinto!')
            user_score += 1
        elif pc_choice == 1:
            print('Il computer ha scelto carta')
            print('E\' patta!')
        else:
            print('Il computer ha scelto forbice')
            print('Il computer vince!')
            pc_score += 1

    if user_input == '0':
        print('Hai scelto sasso')
        pc_choice = random.randint(0, 2)
        if pc_choice == 0:
            print('Il computer ha scelto sasso')
            print('E\' patta!')
        elif pc_choice == 1:
            print('Il computer ha scelto carta')
            print('Il computer vince!')
            pc_score += 1
        else:
            print('Il computer ha scelto forbice')
            print('Hai vinto!')
            user_score += 1
            

