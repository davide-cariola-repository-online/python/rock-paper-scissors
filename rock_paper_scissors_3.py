import random

user_score = 0
pc_score = 0

labels = ['Sasso', 'Carta', 'Forbice']

###### CREIAMO UNA MATRICE
#           PC 0    PC 1    PC 2
# USER 0    patta   pc      user
# USER 1    user    patta   pc
# USER 2    pc      user    patta
#
######

game_matrix = [
    ['patta', 'vince computer', 'vince giocatore'],
    ['vince giocatore', 'patta', 'vince computer'],
    ['vince computer', 'vince giocatore', 'patta']
]

game = True
while game:
    print("0 - Sasso")
    print("1 - Carta")
    print("2 - Forbice")
    print("3 - Resetta il punteggio")
    print("4 - Termina il gioco")

    user_input = input('\nCosa vuoi fare?\n')

    if user_input == '4':
        print('Gioco terminato!')
        print(f"Punteggio giocatore umano: {user_score}")
        print(f"Punteggio computer: {pc_score}")
        if int(user_score) > int(pc_score):
            print("Complimenti! Hai vinto!")
        else:
            print("Mi dispiace! Questa volta non hai vinto...")
        game = False
    
    elif user_input == '3':
        print('Resetto i punteggi!')
        user_score = 0
        pc_score = 0
        print(f"Punteggio giocatore umano: {user_score}")
        print(f"Punteggio computer: {pc_score}")

    elif int(user_input) in range (0, 3):
        pc_choice = random.randint(0, 2)
        user_choice = int(user_input)
        print(f"Hai scelto {labels[user_choice]}")
        print(f"Il computer ha scelto {labels[pc_choice]}")
        print(f"Risultato: {game_matrix[user_choice][pc_choice]}")
        if game_matrix[user_choice][pc_choice] == 'vince computer':
            pc_score += 1
        elif game_matrix[user_choice][pc_choice] == 'vince giocatore':
            user_score += 1

    else:
        print('Scelta non valida')